import React, { Component } from 'react';

import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';

//import Fab from '@material-ui/core/Fab';

import HomeIcon from '@material-ui/icons/Home';

//import GitHubCircleIcon from 'mdi-material-ui/GithubCircle';

import EmptyState from '../EmptyState';

import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';


const styles = (theme) => ({
  emptyStateIcon: {
    fontSize: theme.spacing(12)
  },

  button: {
    marginTop: theme.spacing(1)
  },

  buttonIcon: {
    marginRight: theme.spacing(1)
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 500,

  },
});

class HomeContent extends Component {
  render() {
    // Styling
    const { classes } = this.props;

    // Properties
    const { user } = this.props;
    const { imgData } = this.props;

    if (user) {
      return (
        <EmptyState
          icon={<HomeIcon className={classes.emptyStateIcon} color="action" />}
          title="Home"
        />
      );
    }

    return (
      <div className={classes.root}>
      <GridList cellHeight={180} className={classes.gridList} cols={1} spacing={6}>
        <GridListTile key="Subheader" cols={6} style={{ height: 'auto' }}>
          <ListSubheader component="div">Weed list</ListSubheader>
        </GridListTile>
        {imgData.map(tile => (
          <GridListTile key={tile.location.bucket + '/' + tile.location.path_}>
            <img src={'https://firebasestorage.googleapis.com/v0/b/imagelisting-261c7.appspot.com/o/' + tile.location.path_.replace("/", '%2F') + '?alt=media'} alt={tile.name} />
            <GridListTileBar
              title={tile.title}
              subtitle={<span>by: {tile.name}</span>}
              actionIcon={
                <IconButton aria-label={`info about ${tile.title}`} className={classes.icon}>
                  <InfoIcon />
                </IconButton>
              }
            />
          </GridListTile>
        ))}
      </GridList>
    </div>

    );
  }
}

HomeContent.propTypes = {
  // Styling
  classes: PropTypes.object.isRequired,

  // Properties
  user: PropTypes.object
};

export default withStyles(styles)(HomeContent);
